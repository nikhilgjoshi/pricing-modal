var modal1 = document.getElementById("myModal1");
var modal2 = document.getElementById("myModal2");
var modal3 = document.getElementById("myModal3");

var btn1 = document.getElementById("myBtn1");
var btn2 = document.getElementById("myBtn2");
var btn3 = document.getElementById("myBtn3");

var closeBtn1 = document.getElementById("close1");
var closeBtn2 = document.getElementById("close2");
var closeBtn3 = document.getElementById("close3");

var spanBtn1 = document.getElementById('span-btn-1');
var spanBtn2 = document.getElementById('span-btn-2');
var spanBtn3 = document.getElementById('span-btn-3');

var submit1 = document.getElementById('submit1');
var submit2 = document.getElementById('submit2');
var submit3 = document.getElementById('submit3');

var users = 0; // Default value

// Function to update the displayed users and show/hide modals accordingly
function updateUsers() {
  users = parseInt(document.getElementById('user').value);
  document.getElementById('users').innerHTML = users + ' users included';

  if (users === 0) {
    modal1.style.display = "none";
    modal2.style.display = "none";
    modal3.style.display = "none";
  } else if (users === 10) {
    modal1.style.display = "block";
    modal2.style.display = "none";
    modal3.style.display = "none";
  } else if (users === 20) {
    modal1.style.display = "none";
    modal2.style.display = "block";
    modal3.style.display = "none";
  } else {
    modal1.style.display = "none";
    modal2.style.display = "none";
    modal3.style.display = "block";
  }
}

btn1.onclick = function() {
  modal1.style.display = "block";
}

btn2.onclick = function() {
  modal2.style.display = "block";
}

btn3.onclick = function() {
  modal3.style.display = "block";
}

// When the user clicks on <span> (x) or outside the modal, close the modal
window.onclick = function(event) {
  if (event.target == modal1) {
    modal1.style.display = "none";
  } else if (event.target == modal2) {
    modal2.style.display = "none";
  } else if (event.target == modal3) {
    modal3.style.display = "none";
  }
}

closeBtn1.onclick = function() {
  modal1.style.display = "none";
}

closeBtn2.onclick = function() {
  modal2.style.display = "none";
}

closeBtn3.onclick = function() {
  modal3.style.display = "none";
}

spanBtn1.onclick = function() {
  modal1.style.display = "none";
}

spanBtn2.onclick = function() {
  modal2.style.display = "none";
}

spanBtn3.onclick = function() {
  modal3.style.display = "none";
}

submit1.onclick = function() {
  alert('Form submitted successfully!');
  modal1.style.display = "none";
}

submit2.onclick = function() {
  alert('Form submitted successfully!');
  modal2.style.display = "none";}

submit3.onclick = function() {
  alert('Form submitted successfully!');
  modal3.style.display = "none";
}